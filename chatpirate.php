<?php
/**
 * NOTICE OF LICENSE
 *
 * Please, read this carefully. By using all or any portion of the Software you accept all the terms and
 * conditions of this Agreement. If you do not agree, do not use this Software.
 *
 * 1. DEFINITIONS
 * When used in this Agreement, the following terms shall have the respective
 * meanings indicated, such meanings to be applicable to both the singular and plural forms of the terms defined:
 *
 * “Licensor” means AdviceOnline sp. z o.o.
 *
 * “Licensee” means You or Your Company, unless otherwise indicated.
 *
 * “Software” means (a) all of the contents of the files, disk(s), CD-ROM(s)
 * or other media with which this Agreement is provided, including but not
 * limited to ((i) registration information, i.e. License key which is unique
 * for a registration name of the Licensee; (ii) related explanatory written
 * materials or files (“Documentation”); and (iii) Software setup files and
 * code samples (if any); and (b) upgrades, modified versions, updates, additions,
 * and copies of the Software, if any, licensed to you by AdviceOnline sp. z o.o.
 * (collectively, “Updates”).
 *
 * “Use” or “Using” means to access, install, download, copy or otherwise benefit
 * from using the functionality of the Software in accordance with the Documentation.
 *
 * “System” means Windows OS, GNU/Linux or Mac OS X, or any virtual machine.
 *
 * 2. GENERAL USE
 * You are granted a non-exclusive License to Use the downloaded Software for any purposes
 * for an unlimited period of time.
 *
 * The software product under this License is provided free of charge. Even though a license
 * fee is not paid for the use of such software, it does not mean that there are no conditions for using such software.
 *
 * 2.1. The Software may be installed and Used by the Licensee for any legal purpose.
 *
 * 2.2. The Software may be installed and Used by the Licensee on any number of systems.
 *
 * 2.3. The Software can be copied and distributed under the condition that original
 * copyright notice and disclaimer of warranty will stay intact and the Licensee will
 * not charge money or fees for the Software product, except to cover distribution costs.
 *
 * 2.4. The Licensee will not have any proprietary rights in and to the Software.
 * The Licensee acknowledges and agrees that the Licensor retains all copyrights
 * and other proprietary rights in and to the Software.
 *
 * 2.5 Use within the scope of this License is freeof charge and no royalty
 * or licensing fees shall be paid by the Licensee.
 *
 * 3. INTELLECTUAL PROPERTY RIGHTS
 * 3.1 This License does not transmit any intellectual rights on the Software. The Software
 * and any copies that the Licensee is authorized by the Licensor to make are the intellectual
 * property of and are owned by the Licensor.
 *
 * 3.2 The Software is protected by copyright, including without limitation by Copyright Law
 * and international treaty provisions.
 *
 * 3.3 Any copies that the Licensee is permitted to make pursuant to
 * this Agreement must contain the same copyright and other
 * proprietary notices that appear on or in the Software.
 *
 * 3.4 The structure, organization and code of the Software are the valuable trade secrets and confidential information
 * of the Licensor. The Licensee agrees not to decompile, disassemble or otherwise
 * attempt to discover the source code of the Software.
 *
 * 3.5 Any attempts to reverse-engineer, copy, clone, modify or alter in any way the installer program without
 * the Licensor’s specific approval are strictly prohibited. The Licensee is not authorized to use any plug-in
 * or enhancement that permits to save modifications to a file with software licensed and distributed by the Licensor.
 *
 * 3.6 Trademarks shall be used in accordance with accepted trademark practice, including identification of trademarks
 * owners’ names. Trademarks can only be used to identify printed output produced by the Software and such use of any
 * trademark does not give the Licensee any rights of ownership in that trademark.
 *
 * 4. WARRANTY
 * 4.1 The Licensor warrants that:
 *
 * 4.1.1 The Licensor owns the Software and documentation and/or is in possession of valid and existing
 * licenses that support the terms of this Agreement;
 *
 * 4.1.2 the Software conforms to specifications and functionality as specified in Documentation;
 *
 * 4.1.3 to the best of the Licensor’s knowledge, the Software does not infringe upon or violate
 * any intellectual property right of any third party;
 *
 * 4.1.4 the Software does not contain any routine, intentionally designed by the Licensor to disable a
 * computer program, or computer instructions that may alter, destroy or inhibit the processing environment.
 *
 * 4.2 Except those warranties specified in section 4.1 above, the Software is being delivered
 * to the Licensee “AS IS” and the
 * Licensor makes no warranty as to its use or performance.
 *
 * The Licensor does not and cannot warrant the performance or results the Licensee
 * may obtain by using the Software. The entire risk arising out of use or performance
 * of the Software remains with the Licensee.
 *
 * The Licensor gives no warranty, express or implied, that (i) the Software will be of satisfactory
 * quality, suitable for any particular purpose or for any particular use under specified conditions,
 * notwithstanding that such purpose, use, or conditions may be known to the Licensor; or (ii) that
 * the Software will operate error free or without interruption or that any errors will be corrected.
 *
 * 5. LIMITATION OF LIABILITY
 * In no event will the Licensor be liable for any damages, claims or costs whatsoever or any
 * consequential, indirect, incidental damages, or any lost profits or lost savings, even if
 * the Licensor has been advised of the possibility of such loss, damages, claims or costs or
 * for any claim by any third party.
 *
 * In no event will the Licensee be liable to the Licensor on condition that the Licensee
 * complies with all terms and conditions stated in this License.
 *
 * 6. NON-WAIVER
 * If a portion of this agreement is held unenforceable, the remainder shall be valid.
 * It means that if one section of the Agreement is not lawful, the rest of the Agreement is still in force.
 * A party’s failure to exercise any right under this Agreement will not constitute a waiver of (a) any other terms or
 * conditions of this Agreement, or (b) a right at any time thereafter to require exact and strict compliance with the
 * terms of this Agreement.
 *
 *  @author    AdviceOnline sp. z o.o.
 *  @copyright 20015 AdviceOnline sp. z o.o.
 *  @license   https://chatpirate.com/privacypolicy
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once('ChatPirateApi.class.php');

class ChatPirate extends Module
{
    public $html;
    public $ChatPirateApi;

    public function __construct()
    {
        $this->ChatPirateApi = new ChatPirateApi();

        $this->context = Context::getContext();

        $this->html = '';

        $this->name = 'chatpirate';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'ChatPirate';
        $this->need_instance = 0;
        $this->secure_key = Tools::encrypt($this->name);
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;
        $this->module_key = 'cc99d164a63e911a140ebfc5086afd5e';


        parent::__construct();

        $this->displayName = $this->l('ChatPirate');
        $this->description = $this->l('Quickly add a chat box to your website and start chatting with visitors.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
    }

    public function install()
    {
        if (parent::install() && $this->registerHook('displayFooter')) {
            return true;
        }

        return false;
    }

    private function isEmail($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        } else {
            return true;
        }
    }

    public function uninstall()
    {
        if (parent::uninstall()) {
            $res  = Configuration::deleteByName('chatpirate_companyid');
            $res &= Configuration::deleteByName('chatpirate_token');
            $res &= Configuration::deleteByName('chatpirate_email');

            return (bool)$res;
        }

        return false;
    }

    public function getContent()
    {
        $this->postActions();
        $this->smartyAssign();
        $this->context->controller->addCSS($this->_path . 'views/css/chatpirate.css');
        $this->context->controller->addJS($this->_path . 'views/js/chatpirate.js');

        $this->html .= $this->display(__FILE__, 'views/templates/admin/admin.tpl');

        return $this->html;
    }

    private function smartyAssign()
    {
        $this->context->smarty->assign(
            array(
                'link' => $this->context->link,
                'modname' => $this->name,
                'image_baseurl' => $this->_path . 'views/img/',
            )
        );

        $companyid = Configuration::get('chatpirate_companyid');
        $email =     Configuration::get('chatpirate_email');
        $token =     Configuration::get('chatpirate_token');

        if ($companyid && $email && $token) {
            $this->context->smarty->assign('chatpirate_companyid', $companyid);
            $this->context->smarty->assign('chatpirate_email', $email);
            $this->context->smarty->assign('chatpirate_token', $token);

            $url = $this->ChatPirateApi->CPloginByToken($email, $token);
            $this->context->smarty->assign('chatpirate_url', $url);
        }
    }

    private function postActions()
    {
        $postValidation = $this->postValidation();

        if (!empty($postValidation)) {
            # ERRORS
        } else {

            /*
             * LOGIN
             */
            if (Tools::isSubmit('cp_login_submit')) {

                $this->ChatPirateApi->authSet(Tools::getValue('cp_login'), Tools::getValue('cp_password'));
                $CP_companyShow = $this->ChatPirateApi->CPgetToken();

                if (isset($CP_companyShow->error->message)) {
                    $errors = array($CP_companyShow->error->message);
                    $this->displayErrors($errors);
                } elseif ($CP_companyShow == false) {
                    $this->displayErrors(array('Something went wrong.'));
                } else {
                    if (isset($CP_companyShow->data->companyId) && !empty($CP_companyShow->data->companyId) &&
                        isset($CP_companyShow->data->token) && !empty($CP_companyShow->data->token) &&
                        Tools::isSubmit('cp_login') && Tools::getValue('cp_login') != ''
                    ) {

                        $companyid = $CP_companyShow->data->companyId;
                        $token = $CP_companyShow->data->token;

                        Configuration::updateValue('chatpirate_companyid', $companyid);
                        Configuration::updateValue('chatpirate_token', $token);
                        Configuration::updateValue('chatpirate_email', Tools::getValue('cp_login'));


                        $this->displaySuccess('You have successfully connected your ChatPirate account!');

                    } else {
                        $this->displayErrors(array('An unexpected error occurred.'));
                    }
                }
            }


        }
    }

    private function postValidation()
    {

        $errors = array();

        /*
         * LOGIN
         */
        if (Tools::isSubmit('cp_login_submit')) {

            if (!Tools::isSubmit('cp_login') || Tools::getValue('cp_login') == '') {
                $errors[] = 'E-mail adress can not be empty';
            } else {
                if (!$this->isEmail(Tools::getValue('cp_login'))) {
                    $errors[] = 'Invalid E-mail address';
                }
            }

            if (!Tools::isSubmit('cp_password') ||
                Tools::getValue('cp_password') == '' ||
                Tools::getValue('cp_password') == 'password') {
                $errors[] = 'Password can not be empty';
            } else {
                if (Tools::strlen(Tools::getValue('cp_password')) < 6) {
                    $errors[] = 'Your password must be at least 6 characters';
                }
            }
        }

        return $this->displayErrors($errors);
    }

    public function displayErrors($errors)
    {
        if (!empty($errors)) {
            $html = '';

            foreach ($errors as $error) {
                $html .= $this->displayError($error);
            }

            $this->context->smarty->assign('cp_messages', $html);

            return true;
        } else {
            return false;
        }
    }

    public function displaySuccess($message)
    {
        $html = '<div class="cp_success">'.$message.'</div>';

        $this->context->smarty->assign('cp_messages', $html);
        return true;
    }


    public function hookdisplayFooter()
    {
        $companyid = Configuration::get('chatpirate_companyid');

        if ($companyid) {
            $this->context->smarty->assign('chatpirate_companyid', $companyid);
            return $this->display(__FILE__, 'views/templates/hook/hookFooter.tpl');
        } else {
            return true;
        }
    }
}
