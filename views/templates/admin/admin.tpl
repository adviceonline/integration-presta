{*
* NOTICE OF LICENSE
*
* Please, read this carefully. By using all or any portion of the Software you accept all the terms and
* conditions of this Agreement. If you do not agree, do not use this Software.
*
* 1. DEFINITIONS
* When used in this Agreement, the following terms shall have the respective
* meanings indicated, such meanings to be applicable to both the singular and plural forms of the terms defined:
*
* “Licensor” means AdviceOnline sp. z o.o.
*
* “Licensee” means You or Your Company, unless otherwise indicated.
*
* “Software” means (a) all of the contents of the files, disk(s), CD-ROM(s)
* or other media with which this Agreement is provided, including but not
* limited to ((i) registration information, i.e. License key which is unique
* for a registration name of the Licensee; (ii) related explanatory written
* materials or files (“Documentation”); and (iii) Software setup files and
* code samples (if any); and (b) upgrades, modified versions, updates, additions,
* and copies of the Software, if any, licensed to you by AdviceOnline sp. z o.o.
* (collectively, “Updates”).
*
* “Use” or “Using” means to access, install, download, copy or otherwise benefit
* from using the functionality of the Software in accordance with the Documentation.
*
* “System” means Windows OS, GNU/Linux or Mac OS X, or any virtual machine.
*
* 2. GENERAL USE
* You are granted a non-exclusive License to Use the downloaded Software for any purposes
* for an unlimited period of time.
*
* The software product under this License is provided free of charge. Even though a license
* fee is not paid for the use of such software, it does not mean that there are no conditions for using such software.
*
* 2.1. The Software may be installed and Used by the Licensee for any legal purpose.
*
* 2.2. The Software may be installed and Used by the Licensee on any number of systems.
*
* 2.3. The Software can be copied and distributed under the condition that original
* copyright notice and disclaimer of warranty will stay intact and the Licensee will
* not charge money or fees for the Software product, except to cover distribution costs.
*
* 2.4. The Licensee will not have any proprietary rights in and to the Software.
* The Licensee acknowledges and agrees that the Licensor retains all copyrights
* and other proprietary rights in and to the Software.
*
* 2.5 Use within the scope of this License is freeof charge and no royalty
* or licensing fees shall be paid by the Licensee.
*
* 3. INTELLECTUAL PROPERTY RIGHTS
* 3.1 This License does not transmit any intellectual rights on the Software. The Software
* and any copies that the Licensee is authorized by the Licensor to make are the intellectual
* property of and are owned by the Licensor.
*
* 3.2 The Software is protected by copyright, including without limitation by Copyright Law
* and international treaty provisions.
*
* 3.3 Any copies that the Licensee is permitted to make pursuant to
* this Agreement must contain the same copyright and other
* proprietary notices that appear on or in the Software.
*
* 3.4 The structure, organization and code of the Software are the valuable trade secrets and confidential information
* of the Licensor. The Licensee agrees not to decompile, disassemble or otherwise
* attempt to discover the source code of the Software.
*
* 3.5 Any attempts to reverse-engineer, copy, clone, modify or alter in any way the installer program without
* the Licensor’s specific approval are strictly prohibited. The Licensee is not authorized to use any plug-in
* or enhancement that permits to save modifications to a file with software licensed and distributed by the Licensor.
*
* 3.6 Trademarks shall be used in accordance with accepted trademark practice, including identification of trademarks
* owners’ names. Trademarks can only be used to identify printed output produced by the Software and such use of any
* trademark does not give the Licensee any rights of ownership in that trademark.
*
* 4. WARRANTY
* 4.1 The Licensor warrants that:
*
* 4.1.1 The Licensor owns the Software and documentation and/or is in possession of valid and existing
* licenses that support the terms of this Agreement;
*
* 4.1.2 the Software conforms to specifications and functionality as specified in Documentation;
*
* 4.1.3 to the best of the Licensor’s knowledge, the Software does not infringe upon or violate
* any intellectual property right of any third party;
*
* 4.1.4 the Software does not contain any routine, intentionally designed by the Licensor to disable a
* computer program, or computer instructions that may alter, destroy or inhibit the processing environment.
*
* 4.2 Except those warranties specified in section 4.1 above, the Software is being delivered
* to the Licensee “AS IS” and the
* Licensor makes no warranty as to its use or performance.
*
* The Licensor does not and cannot warrant the performance or results the Licensee
* may obtain by using the Software. The entire risk arising out of use or performance
* of the Software remains with the Licensee.
*
* The Licensor gives no warranty, express or implied, that (i) the Software will be of satisfactory
* quality, suitable for any particular purpose or for any particular use under specified conditions,
* notwithstanding that such purpose, use, or conditions may be known to the Licensor; or (ii) that
* the Software will operate error free or without interruption or that any errors will be corrected.
*
* 5. LIMITATION OF LIABILITY
* In no event will the Licensor be liable for any damages, claims or costs whatsoever or any
* consequential, indirect, incidental damages, or any lost profits or lost savings, even if
* the Licensor has been advised of the possibility of such loss, damages, claims or costs or
* for any claim by any third party.
*
* In no event will the Licensee be liable to the Licensor on condition that the Licensee
* complies with all terms and conditions stated in this License.
*
* 6. NON-WAIVER
* If a portion of this agreement is held unenforceable, the remainder shall be valid.
* It means that if one section of the Agreement is not lawful, the rest of the Agreement is still in force.
* A party’s failure to exercise any right under this Agreement will not constitute a waiver of (a) any other terms or
* conditions of this Agreement, or (b) a right at any time thereafter to require exact and strict compliance with the
* terms of this Agreement.
*
*  @author    AdviceOnline sp. z o.o.
*  @copyright 20015 AdviceOnline sp. z o.o.
*  @license   https://chatpirate.com/privacypolicy
*}
<div id="chatpirate" class="wrap">

    <div class="panel">

        <div class="logo">
            <img src="{$image_baseurl|escape:'htmlall':'UTF-8'}logo.png" alt="ChatPirate" />
        </div>

        {if isset($chatpirate_token) && !empty($chatpirate_token) &&
        isset($chatpirate_email) && !empty($chatpirate_email) &&
        isset($chatpirate_url) && !empty($chatpirate_url) && !isset($cp_messages)}
            <div class="cp_success">Your ChatPirate account is connected to your site.</div>
        {/if}

        {if isset($cp_messages)}
            {foreach $cp_messages as $cp_message}
                {$cp_message} {* HTML, cannot escape*}
            {/foreach}
        {/if}

        {if isset($chatpirate_token) && !empty($chatpirate_token) &&
        isset($chatpirate_email) && !empty($chatpirate_email) &&
        isset($chatpirate_url) && !empty($chatpirate_url)}

            <div class="btn_holder"><a href="{$chatpirate_url|escape:'htmlall':'UTF-8'}" class="btn_chatpirate_login" target="_blank">{l s='Go to app' mod='chatpirate'}</a></div>

            <span class="logout">{l s='Sign in to a different account?' mod='chatpirate'} <div id="logout">{l s='Log out' mod='chatpirate'}</div></span>
        {else}
            <h2>{l s='Connect your ChatPirate account with your PrestaShop' mod='chatpirate'}</h2>
        {/if}

    {if isset($chatpirate_token) && !empty($chatpirate_token) &&
    isset($chatpirate_email) && !empty($chatpirate_email) &&
    isset($chatpirate_url) && !empty($chatpirate_url)}
        <div id="hide_login">
    {/if}

            <div id="loginbox">
                <form id="login-form" class="content" action="{$link->getAdminLink('AdminModules')|escape:'htmlall':'UTF-8'}&configure={$modname|escape:'htmlall':'UTF-8'}&token={$token|escape:'htmlall':'UTF-8'}&submit_createaccount" method="POST" enctype="multipart/form-data" novalidate>
                    <div class="form-group">
                        <span>{l s='E-mail:' mod='chatpirate'}</span>
                        <input type="text" name="cp_login" value="">
                    </div>
                    <div class="form-group">
                        <span>{l s='Password:' mod='chatpirate'}</span>
                        <input type="password" name="cp_password" value="">
                    </div>

                    <input class="button" name="cp_login_submit" type="submit" value="Connect">
                </form>
            </div>

            {if isset($chatpirate_token) && !empty($chatpirate_token) &&
            isset($chatpirate_email) && !empty($chatpirate_email) &&
            isset($chatpirate_url) && !empty($chatpirate_url)}
                <p class="signup">{l s='Create new ChatPirate account ' mod='chatpirate'}<a target="_blank" href="//chatpirate.com/signup/createdType/prestashop">{l s='Sign up' mod='chatpirate'}</a></p>
            {else}
                <p class="signup">{l s='Don\'t have an account, yet? ' mod='chatpirate'}<a target="_blank" href="//chatpirate.com/signup/createdType/prestashop">{l s='Sign up' mod='chatpirate'}</a></p>
            {/if}

    {if isset($chatpirate_token) && !empty($chatpirate_token) &&
    isset($chatpirate_email) && !empty($chatpirate_email) &&
    isset($chatpirate_url) && !empty($chatpirate_url)}
        </div>
    {/if}

    </div>
</div>